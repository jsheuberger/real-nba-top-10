# Real NBA Top 10

Every day the NBA publishes a top 10 video of the top plays from last night's games on YouTube. However nobody agrees on what the top 10 should actually be.

Let’s create a site that publishes a top 10 as voted by the users.

## General idea

Create a calendar based application that shows the top 10 plays every day, with the focus being on the most recent day that had NBA games.

Allow scrolling through the different days, and viewing the top 10 plays for those days.
Show the plays that are outside the top 10.
Allow voting on the top 10 using a ranked voting system.

## Contribution

This project uses [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/). 

The repository uses a feature branching strategy, and pull requests are merged as is.

## License
This code is released under MIT License.
