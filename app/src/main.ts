import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { ApolloClient, InMemoryCache } from '@apollo/client/core'
import { DefaultApolloClient } from '@vue/apollo-composable'

import App from './App.vue'
import router from './router'

const app = createApp(App)

const uri = 'https://8000-jsheuberger-realnbatop1-1wnibyp4mb2.ws-eu110.gitpod.io/graphql'
// const uri = 'http://127.0.0.1:8000/graphql'

const apolloClient = new ApolloClient({
    uri: uri, // local GraphQL server
    cache: new InMemoryCache(),
    
})

app.provide(DefaultApolloClient, apolloClient)
app.use(createPinia())
app.use(router)

app.mount('#app')
