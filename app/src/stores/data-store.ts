import { ApplicationStore } from "./application.store";
import { Observable, Subject, from, map, mergeMap, share, take, tap, timer } from 'rxjs';
import gql from "graphql-tag";
import { ApolloClient, type NormalizedCacheObject } from "@apollo/client/core";

export type Queries = {
  [key: string]: any
}

type QueryResult = {
  [key: string]: Observable<any>
}

export abstract class DataStore {

  protected results: QueryResult = {}
  protected queries: Queries = {}

  $trigger: Subject<object> = new Subject()

  constructor(private client: ApolloClient<NormalizedCacheObject>, private name: string) {
    const obj = this.onCreate()
    this.queries = obj

    const req = this.$trigger.pipe(
      map(variables => {  //call the service queries with the variables as a parameter
        const query = gql(this.buildQuery())

        return this.client.query({ 
          query: query, 
          variables: variables,
          fetchPolicy: 'no-cache'
        })
      }),
      mergeMap(data => from(data)),
      share()
    )

    // create derivative observables for each input query
    for(const key in obj) {
      this.results[key] = req.pipe(
        map(data => {}) // TODO parse the matching element from the returned data
      )
    }
  }

  abstract onCreate() : Queries

  get<Type>(key: string) : Observable<Type> {
    // find the matching observable from the created queries
    return <Observable<Type>> this.results[key]
  }

  load(variables: object = {}) : void {
    this.$trigger.next(variables)
  }

  private buildQuery() : string {

    const query = 'query AppQuery {\n' +
    Object.keys(this.queries).map(key => {
      return `${key}: ${this.queries[key]}`
    }).join('\n') +
    '\n}'

    console.log(query)

    return query

    // const test = 'dailyTopPlays'

    // return `query MyQuery($date: String!) {
    //   ${test}: games {
    //     games(filter: {date: $date}) {
    //       id
    //       home
    //       away
    //       date
    //     }
    //   }
    // }`
  }
} 