import type { Highlight } from '@/model';
import { Observable, Subject, tap, timer } from 'rxjs';

export type HighlightList = {
    name: string;
    period: string;
    type: string;
    items: Highlight[]
}

const HIGHLIGHT_LIST = {
    name: "Daily top plays",
    period: "Today",
    type: "daily_top_plays",
    items: [{
        id: '10',
        url: 'https://videos.nba.com/nba/pbp/media/2024/04/09/0022301155/432/07729d83-fee8-385c-3642-810a549d22c4_1280x720.mp4',
        game: {
          name: "GSW @ LAL",
          date: "2024-04-11",
        },
        description: 'Lebron James dunks alley-oop in fast break.',
        players: ['Lebron James', 'Steph Curry', 'Draymond Green'],
        labels: ['Dunk', 'Alley-oop'],
        votes: 1845
      },{
        id: '11',
        url: 'https://videos.nba.com/nba/pbp/media/2024/04/09/0022301155/432/07729d83-fee8-385c-3642-810a549d22c4_1280x720.mp4',
        game: {
          name: "GSW @ LAL",
          date: "2024-04-11",
        },
        description: 'Another highlight',
        players: ['Lebron James', 'Steph Curry', 'Draymond Green'],
        labels: ['Dunk', 'Alley-oop'],
        votes: 1845
      }]
}

export class ApplicationStore {
    
    $dailyTopPlays = new Subject<HighlightList>();

    loadDailyTopPlays() {
        
        timer(2000, -1).pipe(
            tap(() => {
                this.$dailyTopPlays.next(HIGHLIGHT_LIST)
            })
        ).subscribe()
    }
}