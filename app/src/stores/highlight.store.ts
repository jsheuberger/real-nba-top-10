import { ref, computed, inject } from 'vue'
import { defineStore } from 'pinia'
import { HighlightService } from '@/services/highlight.service'

export const useHighlightsStore = defineStore('highlights', {
  state: () => ({
    
  }),
  getters: {
    
  },
  actions: {
    selectDate() {
      
    },
    selectPeriod() {
      
    }
  }
})