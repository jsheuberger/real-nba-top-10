
class ObjectBuilder {

    name: string
    fields: string[] = []

    constructor(name: string, private qb: QueryBuilder) {
        this.name = name
    }

    field(name: string) : ObjectBuilder {
        this.fields.push(name)
        return this
    }

    close() : QueryBuilder {
        return this.qb
    }
}

export class QueryBuilder {



    query(name: string, alias: string) : QueryBuilder {
        return this
    }

    field(name: string) : QueryBuilder {
        return this
    }

    object(name: string) : ObjectBuilder {
        return new ObjectBuilder(name, this)
    }

    filter(name: string, value: string) : QueryBuilder {
        return this
    }

    static qb() : QueryBuilder {
        return new QueryBuilder()
    }
}

const query = QueryBuilder.qb()
    .query('highlights', 'dailyTopPlays')
    .filter('type', 'all')
    .filter('date', '2024-04-11')
    .field('')
    .field('')