export type Game = {
    name: string;
    date: string;
}