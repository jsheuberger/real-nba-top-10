import type { Game } from "@/model/game";

export type Highlight = {
    id: string;
    url: string;
    game: Game;
    description: string;
    players: string[];
    labels: string[];
    votes: number;
};
