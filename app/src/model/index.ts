export type { Game } from "@/model/game";
export type { Highlight } from "@/model/highlight";
export type { Player } from "@/model/player";

