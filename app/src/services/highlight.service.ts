import type { Highlight, Game } from "@/model";
import { BaseService } from "./base.service";

const HIGHLIGHTS: Highlight[]  = [
    {
        id: '1234',
        url: 'https://videos.nba.com/nba/pbp/media/2024/04/09/0022301155/432/07729d83-fee8-385c-3642-810a549d22c4_1280x720.mp4',
        game: { name: 'GSW @ LAL', date: '2024-04-11' },
        description: "Lebron James dunks alley-oop in fast break.",
        players: ['Lebron James', 'Steph Curry', 'Draymond Green'],
        labels: ['dunk', 'alley-oop'],
        votes: 12345
    },
    {
        id: '5678',
        url: 'https://videos.nba.com/nba/pbp/media/2024/04/09/0022301155/432/07729d83-fee8-385c-3642-810a549d22c4_1280x720.mp4',
        game: { name: 'DET @ PHI', date: '2024-04-11' },
        description: "Tyrese Maxey shoots a deep 3pt in the 3rd quarter.",
        players: ['Tyrese Maxey', 'Cade Cunningham', 'Draymond Green'],
        labels: ['clutch', '3pt'],
        votes: 7654
    },
];

export class HighlightService extends BaseService {

    getDailyTopPlays(date: string | undefined): Highlight[] {
        // use API to get the top plays from the last games, or the specified date
        return HIGHLIGHTS
    }

    getPeriodicTopPlays(period: string, date: string | undefined, labels: string[]): Highlight[] {
        return HIGHLIGHTS.filter(highlight => highlight.labels.some(label => labels.includes(label)))
    }
}