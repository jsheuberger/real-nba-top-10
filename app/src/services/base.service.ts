import { ApolloClient } from "@apollo/client/core";

export class BaseService {

    constructor(private graphql: ApolloClient<any>) {
        
    }
}