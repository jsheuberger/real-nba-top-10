# Data model

We identify the following model entities:
- Game
- Highlight
- Player

## Game

The game represents an NBA basketball game, played on a certain date at a certain venue.

It contains the following properties:
- Id
- Home: Home team identifier, e.g. GSW
- Away: Away team identifier, e.g. LAL
- Date: Date the game took place
- Type: Regular season, or Playoff round: e.g. Round_1
- Highlights: List of highlights that happened in the game

## Highlight

A highlight represents an action that happened in a specific game.
- Id
- URL: url to the NBA video for this highlight
- Players: List of players involved in the highlight, e.g. score, assist, defender, etc
- Labels: List of labels that describe the highlight, e.g. dunk, alley-oop, block, circus, clutch, record, etc
- Game: The game in which the highlight happened
- Score: The popularity of the highlight

## Player

A player that was involved in a specific highlight. This exists as an entity mainly for searching:
- Id
- Firstname
- Lastname

Note we are leaving the team and number out, because players can switch teams/numbers during seasons.



