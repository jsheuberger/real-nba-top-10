# GraphQL Design

The application will use a GraphQL endpoint to access the relevant data.

## GraphQL Queries

- Highlights
- Games
- Players

To display the top highlights the application needs to retrieve a list of the highlights with specific filters:
- Filter by date
- Limit the result set
- Sort the result by score

To list the top highlights for a specific period/type (Top dunks of the week, etc):
- Filter by period
- Filter by label

To list the games on a specific date:
- Filter by date

To find what days had games:
- Get number of games per day
- Get games per date

To list the players for search filters:
- Filter/Search by name

## GraphQL Mutations

- Vote for a highlight
- Nominate a highlight for a game
