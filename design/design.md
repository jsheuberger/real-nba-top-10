# Top Level Design Document

The Real NBA Top 10 Application will consist of the following components:
- A web-application front-end to display the top 10 to the public
- A GraphQL back end to serve the results to the application
- A firestore data store to serve the stored data

## Technology Stack

The application will be developed in [Vue.js](https://vuejs.org/).

The graphql back-end will be created in Python using [Strawberry](https://strawberry.rocks/).

The data will be stored in a no-sql data store.

The application will be deployed as a Google Cloud Run container.

## Functional Design

The following is a list of personas and use cases for the application:

Personas:
- Dray: An opinionated NBA basketball fan
- Lily: The NBA Real Top 10 application administrator

User stories are written in the form: As [Persona], I want [Functionality], so that [UseCase].

### User Stories

As Dray, I want to see today's Top plays so that I can see what exciting plays happened during yesterday's basketball games.
As Dray, I want to watch the whole top 10 list in one go.
As Dray, I want to vote for my favourite play because I think it wasn’t high enough on the list
As Dray, I want to nominate a play that I saw while watching the game, but that wasn’t on the list.
As Dray, I want to see the top plays from other days, because I forgot to check them on the day itself.
As Dray, I want to see the top plays of the week/month/year/season/playoffs/etc, so that we can 
As Dray, I want to classify the plays as dunks, assists, blocks, end-to-end, clutch, game winners, etc, so that I can filter out plays that I like.
As Dray, I want to label a play with the players involved so that I can filter out plays from my favourite players.
As Dray, I want to comment on a top plays list, so that I can explain why I think the list is wrong, or express why the top play is so amazing.

As Lilly, I want to prevent the same person from voting for the same play multiple times as much as possible, because that would invalidate the top plays list.
As Lilly, I want to moderate the comments, so that I can step in if discussions become unpleasant or toxic.
As Lilly, I want to update player labels, and categories so that I can fix problems if they arise.


