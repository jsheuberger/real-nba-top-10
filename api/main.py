import os
from functools import cached_property
from model import Query, Mutation
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from strawberry import Schema
from strawberry.fastapi import GraphQLRouter, BaseContext
from firebase_admin import firestore, initialize_app
from services import GameService, PlayerService

PROJECT_ID = os.environ.get('PROJECT_ID', '')

def app():
    """
    Function to create and return a FastAPI app with a GraphQL router.
    """

    # create the dependencies
    firebase_app = initialize_app()
    firestore_client = firestore.Client(project=PROJECT_ID)
    game_service = GameService(firestore_client)
    player_service = PlayerService(firestore_client)
    # highlight_service = HighlightService(firestore_client)

    
    context = {
        "services": {
            "firestore": firestore_client,
            "games": game_service,
            "players": player_service,
            "highlights": None
        }
    }

    schema = Schema(query=Query, mutation=Mutation)

    async def get_context(): 
        return context

    graphql_app = GraphQLRouter(schema, context_getter=get_context)
    fastapi = FastAPI()
    # TODO get the origin headers from config
    fastapi.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_headers=["*"],
        allow_methods=["*"],
        allow_credentials=True
    )
    fastapi.include_router(graphql_app, prefix="/graphql")
    return fastapi

# Manual entrypoint to start the uvicorn server
if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", action="store_true")
    parser.add_argument("--host", default="127.0.0.1")
    parser.add_argument("--port", type=int, default=8000)
    args = parser.parse_args()

    import uvicorn

    # cfg = uvicorn.Config(app=app, host=args.host, port=args.port, log_level="debug", factory=True, reload=args.debug)
    # server = uvicorn.Server(cfg)
    # server.run()
    uvicorn.run("main:app", host=args.host, port=args.port, log_level="debug", factory=True, reload=args.debug)