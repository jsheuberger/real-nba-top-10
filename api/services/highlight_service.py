from model.highlight import Highlight
from firebase_admin import firestore
from .game_service import GAMES_COLLECTION

HIGHLIGHTS_COLLECTION = "highlights"

class HighlightService:

    def __init__(self, firestore: firestore.Client):
        self.firestore = firestore

    def get_highlights(self, label: str = None, player: id = None, date: str = None, limit: int = 10, order_by: str = None) -> list[Highlight]:
        highlights_collection_group = self.firestore.collection_group(HIGHLIGHTS_COLLECTION).where("status", "==", "final")
        if label:
            highlights_collection_group.where("labels", "array-contains", label)
        if player:
            highlights_collection_group.where("players", "array-contains", player.id)
        if date:
            highlights_collection_group.where("date", "==", date)
        if order_by:
            highlights_collection_group.order_by(order_by, direction=firestore.Query.DESCENDING)

        return [Highlight.from_dict(doc.to_dict()) for doc in highlights_collection_group.limit(limit).stream()]

    def suggest_highlight(self, highlight: Highlight) -> Highlight:
        game_id = highlight.game.id
        highlights_ref = self.firestore.collection(GAMES_COLLECTION).document(game_id).collection(HIGHLIGHTS_COLLECTION)

        hl_ref = highlights_ref.add(highlight.to_dict())
        return Highlight.from_dict(hl_ref.get().to_dict())

    def approve_highlight(self, highlight: Highlight):
        game_id = highlight.game.id
        hl_ref = self.firestore.collection(GAMES_COLLECTION).document(game_id).collection(HIGHLIGHTS_COLLECTION).document(highlight.id)
        hl_ref.update({"status": "final"})

    def score_highlight(self, highlight: Highlight, increment: int):
        game_id = highlight.game.id
        hl_ref = self.firestore.collection(GAMES_COLLECTION).document(game_id).collection(HIGHLIGHTS_COLLECTION).document(highlight.id)
        hl_ref.update({"score": firestore.Increment(increment)})