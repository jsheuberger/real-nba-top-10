from firebase_admin import firestore, initialize_app

class ServiceManager:

    def __init__(self, services: dict = {}):
        self.services = services

    @property
    def firestore(self):
        def create_firestore():
            initialize_app()
            return firestore.Client()
        return get_or_create("firestore", create_firestore)

    @property
    def games(self):
        return get_or_create("games", lambda: GameService(self.firestore))

    @property
    def highlights(self):
        return self.services.get("highlights")

    def get_or_create(self, service_name, constructor: callable):
        if(self.services.get(service_name) is None):
            self.services[service_name] = constructor()
        return self.services.get(service_name)