from model.game import Game
from firebase_admin import firestore
from firebase_admin.firestore import FieldFilter

GAMES_COLLECTION = "games"

class GameService:

    def __init__(self, firestore: firestore.Client):
        self.firestore = firestore

    def get_games(self, home: str = None, away: str = None, date: str = None) -> list[Game]:
        games_ref = self.firestore.collection(GAMES_COLLECTION)
        if home:
            games_ref.where(filter=FieldFilter("home", "==", home))
        if away:
            games_ref.where(filter=FieldFilter("away", "==", away))
        if date:
            games_ref.where(filter=FieldFilter("date", "==", date))
        
        return [Game.from_dict(doc.id, doc.to_dict()) for doc in games_ref.stream()]

    def add_game(self, game: Game):
        ref = self.firestore.collection(GAMES_COLLECTION).document(game.id)
        ref.set(game.to_dict())