from model.player import Player
from firebase_admin import firestore
from firebase_admin.firestore import FieldFilter, Or

PLAYERS_COLLECTION = "players"

class PlayerService:
    def __init__(self, firestore: firestore.Client):
        self.firestore = firestore

    def get_players(self, first_name: str = None, last_name: str = None, search: str = None) -> list[Player]:
        players_ref = self.firestore.collection(PLAYERS_COLLECTION)
        
        filters = []
        if first_name:
            filters.append(FieldFilter("first_name", "==", first_name))
        if last_name:
            filters.append(FieldFilter("last_name", "==", last_name))
        if search:
            filters.append(Or(FieldFilter("first_name", ">=", search),FieldFilter("first_name", "<=", search + "\uf8ff")))

        query = players_ref.where(filter=Or(*filters))

        return [Player.from_dict(doc.id, doc.to_dict()) for doc in query.stream()]

    def add_player(self, player: Player) -> Player:
        ref = self.firestore.collection(PLAYERS_COLLECTION).document(player.id)
        ref.set(player.to_dict())
        return player