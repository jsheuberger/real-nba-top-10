import typing
import strawberry

from model.game import GameQueries
from model.highlight import HighlightQueries
from model.player import PlayerQueries, PlayerMutations

@strawberry.type
class Query():
    
    @strawberry.field(permission_classes=[])
    def games(self) -> GameQueries:
        return GameQueries()
    
    # @strawberry.field
    # def highlights(self) -> HighlightQueries:
    #     return HighlightQueries()

    @strawberry.field
    def players(self) -> PlayerQueries:
        return PlayerQueries()

@strawberry.type
class Mutation:

    @strawberry.field
    def players(self) -> PlayerMutations:
        return PlayerMutations()