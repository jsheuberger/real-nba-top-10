import strawberry
import typing
from enum import Enum

@strawberry.type
class PagingWindow:
    count: typing.Optional[int] = None
    offset: typing.Optional[int] = None
    sort: typing.Optional[typing.List[str]] = None

Item = typing.TypeVar("Item")

@strawberry.type
class PaginationWindow(typing.Generic[Item]):
    items: typing.List[Item]
    total_items: int

class SortOrder(Enum):
    ASC = "asc"
    DESC = "desc"

class SortField:
    field: typing.Optional[str] = None
    direction: typing.Optional[SortOrder] = None

class Sorting:
    fields: typing.List[SortField] = []