import strawberry
import typing

@strawberry.type
class Player():
    id: strawberry.ID
    first_name: str
    last_name: str
    
    def to_dict(self) -> dict:
        return {
            "id": self.id,
            "first_name": self.first_name,
            "last_name": self.last_name,
        }
    
    @staticmethod
    def from_dict(id: str, data: dict) -> "Player":
        return Player(
            id=id,
            first_name=data["first_name"],
            last_name=data["last_name"]
        )
    
    def __repr__(self):
        return f"Player(id={self.id}, first_name={self.first_name}, last_name={self.last_name})"

@strawberry.input
class PlayerFilter():
    first_name: typing.Optional[str] = None
    last_name: typing.Optional[str] = None
    search: typing.Optional[str] = None

@strawberry.type
class PlayerQueries():
    
    @strawberry.field(description="Get all players")
    def all(self, info: strawberry.Info = None) -> list[Player]:
        player_service = info.context.get("services").get("players")
        return player_service.get_players()
    
    @strawberry.field(description="Get filtered list of players")
    def filter(self, info: strawberry.Info = None, first_name: str = None, last_name: str = None, search: str = None) -> list[Player]:
        player_service = info.context.get("services").get("players")
        return player_service.get_players(first_name=first_name, last_name=last_name, search=search)

@strawberry.type
class PlayerMutations():

    @strawberry.mutation(description="Add a player to the database")
    def create(self, info: strawberry.Info, id: str, first_name: str, last_name: str) -> Player:
        player_service = info.context.get("services").get("players")
        return player_service.add_player(Player(id=id, first_name=first_name, last_name=last_name))
