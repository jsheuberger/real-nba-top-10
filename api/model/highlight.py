import strawberry
import typing

from model.player import Player
from model.game import Game

@strawberry.type
class Highlight:
    """A highlight from a game."""

    id: strawberry.ID
    url: str
    players: typing.List[Player]
    labels: typing.List[str]
    game: Game
    score: int
    status: strawberry.Private[str] = "pending"
    
    def to_dict(self):
        return {
            "id": self.id,
            "url": self.url,
            "players": [p.to_dict() for p in self.players],
            "labels": self.labels,
            "game": self.game.to_dict(),
            "score": self.score,
            "status": self.status
        }
    
    @staticmethod
    def from_dict(data):
        return Highlight(
            id=data["id"],
            url=data["url"],
            players=[Player.from_dict(p) for p in data["players"]],
            labels=data["labels"],
            game=Game.from_dict(data["game"]),
            score=data["score"],
            status=data["status"]
        )

    def __repr__(self):
        return f"Highlight(id={self.id}, url={self.url}, players={self.players}, labels={self.labels}, game={self.game}, score={self.score}, status={self.status})"

class HighlightQueries:

    @strawberry.field
    def highlights(self, info: strawberry.Info) -> typing.List[Highlight]:
        context = info.context
        highlight_service = context.services.get_highlights()
        return highlight_service.get_highlights()