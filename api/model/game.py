import strawberry
import typing

@strawberry.type
class Game():
    id: strawberry.ID
    home: str
    away: str
    date: str
    highlights: list[str]

    def to_dict(self) -> dict:
        return {
            "id": self.id,
            "home": self.home,
            "away": self.away,
            "date": self.date
        }

    @staticmethod
    def from_dict(id: str, data: dict) -> "Game":
        return Game(
            id=id,
            home=data["home"],
            away=data["away"],
            date=data["date"],
            highlights=[]
        )
    
    def __repr__(self) -> str:
        return f"Game(id={self.id}, home={self.home}, away={self.away}, date={self.date})"

@strawberry.input
class GameFilter():
    team: typing.Optional[str] = None
    home: typing.Optional[bool] = None
    away: typing.Optional[bool] = None
    date: typing.Optional[str] = None

@strawberry.type
class GameQueries:
    @strawberry.field
    def games(self, info: strawberry.Info, filter: typing.Optional[GameFilter] = None) -> list[Game]:
        game_service = info.context.get("services").get("games")
        return game_service.get_games(filter.home, filter.away, filter.date)

@strawberry.type
class GameMutations:

    @strawberry.mutation
    def add_game(self, info: strawberry.Info, game: Game) -> Game:
        game_service = info.context.get("services").get("games")
        return game_service.add_game(game)