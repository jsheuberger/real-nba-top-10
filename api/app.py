import strawberry
import typing

from functools import cached_property
from fastapi import FastAPI
from strawberry.fastapi import GraphQLRouter, BaseContext

class AuthContext: 

    def __init__(self, authenticated = False, system = False, username = None, token = None):
        self.system = system
        self.authenticated = authenticated
        self.username = username
        self.token = token

    def is_authenticated(self) -> bool:
        return self.authenticated

    def is_system(self) -> bool:
        return self.system

class Context(BaseContext):
    @cached_property
    def auth(self) -> AuthContext | None:
        if not self.request:
            return None

        auth_header = self.request.headers.get("Authorization", None)
        if not auth_header:
            return AuthContext()

        token = auth_header.replace("Bearer ", "")
        return AuthContext(True, token=token, username=token)

async def get_context():
    return Context()

@strawberry.input
class User:
    """
    User input class with a name and an Id field
    """
    id: strawberry.ID
    name: str

@strawberry.type
class Query():
    @strawberry.field
    def hello(self, user: typing.Optional[User] = None) -> str:
        if not user:
            return "World"
        return user.name

    @strawberry.field
    def auth(self, info: strawberry.Info = None) -> str | None:
        if not info.context.auth.is_authenticated():
            raise Exception("Unauthenticated")
        return info.context.auth.username

def app():
    schema = strawberry.Schema(query=Query)
    graphql_app = GraphQLRouter(schema, context_getter=get_context)
    app = FastAPI()
    app.include_router(graphql_app, prefix="/graphql")
    return app